Hygieia

Pre-requisites to configure hygieia dashboard.

	1. Java
	2. Apache Maven
	3. Eclipse IDE
	4. MongoDB
	5. NodeJs
	6. Git

You have to do the following steps to configure it.
1. Open Hygieia\UI on command line

2. Issue the following commands via command line:
	npm install -g bower
	npm install -g gulp

3. Navigate to your project root(Hygieia\UI) via command line and use the following command:
	npm install

4. Use Git Shell to install bower in the following manner; do so from your project�s root directory:
	bower install

5. Start the UI by running gulp serve on Hygieia\UI folder.

6. Open http://localhost:3000/ from browser, it will display the Dashboard

But API is not connected yet.

1.Open Eclipse IDE

2.File -> Import -> Maven-> Existing Maven Projects, browse Downloaded Hygieia folder->  Finish

3. update maven projects.

4. start mongodb now.

Now you have to start the api.

1. create a dashboard.properties file inside Hygieia\api

2. it will resemble as follows.

	dbname=dashboarddb
	# dbusername=[MogoDb Database Username, defaults to empty]
	# dbpassword=[MongoDb Database Password, defaults to empty]
	dbhost=localhost
	dbport=27017 

3. create a application.properties file inside Hygieia\api\src\main\resources

4. it will resemble as follows.

	# Default server configuration values
	dbname=dashboard
	# dbusername=dashboarduser
	# dbpassword=1qazxSw2
	dbhost=127.0.0.1
	dbport=11500
	server.contextPath=/api
	server.port=8080
5. Now do the following step in eclipse.
	api project ? Go to src ? com.capitalone.dashboard.Application.java ?Rightclick?Run Configuartions ? Java Application ? go to arguments ? program args : add
	--spring.config.location=dashboard.properties

If you see the message �Tomcat started on port(s): 8080 (http)� means api is running successfully.

Now you have to start the UI.

1. Open nodejs command prompt, navigate to UI folder location & run gulp serve command

2. Open http://localhost:3000/#/
3. Signup with username & password (minimum 6 characters); Ex: admin & 123456


Now navigate to the collectors folder and create a application.properties file in the root of collector you want to configure.

Below is a sample collector configuration for jenkins.

#Database Name
dbname=dashboarddb

#Database HostName - default is localhost
#dbhost=localhost

#Database Port - default is 27017
#dbport=9999

#Database Username - default is blank
#dbusername=db

#Database Password - default is blank
#dbpassword=dbpass

jenkins.servers[0]=http://localhost:8181/jenkins/
#Collector schedule (required)
jenkins.cron=0 0/1 * * * *

#Jenkins server (required) - Can provide multiple

#If using username/token for api authentication (required for Cloudbees Jenkins Ops Center) see sample
#jenkins.servers[1]=http://username:token@jenkins.company.com

#Another option: If using same username/password Jenkins auth - set username/apiKey to use HTTP Basic Auth (blank=no auth)
#jenkins.username=
#jenkins.apiKey=

#Determines if build console log is collected - defaults to false
jenkins.saveLog=true 
