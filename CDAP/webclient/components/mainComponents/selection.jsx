const React = require('react');
const {hashHistory} = require('react-router');
import Card from './Card.jsx';
const ReactToastr = require('react-toastr');
const {ToastContainer} = ReactToastr;
const ToastMessageFactory = React.createFactory(ReactToastr.ToastMessage.animation);

class Selection extends React.Component {
  constructor() {
    super();
    this.state = {
      rootPath: '',
      collectorsArray: []
    };
  }

  componentWillMount(){
    this.getCollectors();
    this.getRootPath();
  }

  getCollectors(){
    $.ajax({
       url:"/restart/getCollectors",
       type: 'GET',
       success: function(res)
       {

         let collectorsArray = [];
         res.map((item,i)=>{
           collectorsArray.push({
             name: item.name,
             image: item.image,
             lastRefreshedTime: item.lastRefreshedTime,
             selected: item.selected
           })
         })
         this.setState({collectorsArray:collectorsArray});
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }

  getRootPath(){
    $.ajax({
       url:"/rootPath/getRootPath",
       type: 'GET',
       success: function(res)
       {
         if(res.length != 0){
           this.setState({rootPath:res[0].rootPath});
         }
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }

  rootPath(e){
    this.setState({rootPath:e.target.value});
  }

  toggle(key){
    let collectorsArray = this.state.collectorsArray;
    collectorsArray[key].selected = !collectorsArray[key].selected;
    this.setState({collectorsArray:collectorsArray},function(){
      console.log('aaa',this.state.collectorsArray[key].selected);
    });
  }

  setRootPath(){
    let rp = this.state.rootPath.replace(/\\/g,'/');
    let context =  this;
    console.log('rrrrr',rp);
    $.ajax({
       url:"/rootPath/setRootPath",
       type: 'POST',
       data: {
         rootPath: rp
       },
       success: function(res)
       {
         console.log(res);
         if(res != 'invalid path'){
           this.setSelection();

         }
         else{
           this.setRootPathsFailureAlert();
         }
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure',err);
       }.bind(this)
     });
  }

  setSelection(){
    let context = this;
    console.log('aaaa',context.state.collectorsArray);
    $.ajax({
       url:"/restart/setSelection",
       type: 'POST',
       data:{
         collectorsArray:JSON.stringify(context.state.collectorsArray)
       },
       traditional: true,
       success: function(res)
       {
         this.setRootPathsSuccessAlert();
       }.bind(this),
       error: function(err)
       {
         console.log('inside failure');
         //console.log(err.responseText);
       }.bind(this)
     });
  }

  setRootPathsSuccessAlert() {
    //console.log("inside check for Scenario updated successfully alert");
    let context = this;
    this.refs.asd.success(
      'Root path set successfully',
      '', {
      timeOut: 3000,
      extendedTimeOut: 3000
    }
  );
  setTimeout(function() {
    hashHistory.push('/configure');
  },3000)
}

setRootPathsFailureAlert() {
  //console.log("inside check for Scenario updated successfully alert");
  let context = this;
  this.refs.asd.error(
    'Invalid Root path',
    '', {
    timeOut: 3000,
    extendedTimeOut: 3000
  }
);
}

  render() {
        return(
          <div>
            <nav className="navbar navbar-inverse navbar-fixed-top">
                <div className="container-fluid">
                  <div className="navbar-header">
                    <img src="./../../image/CDAPLogo.png" className="img-responsive" alt="CDAP" style={{float:'left',width:'13%'}}/>
                    <a className="navbar-brand" href="#" style={{color:'white',margin:'1.5% 0 0 1%',fontSize:'20px'}}>Continuous Delivery Analytics Platform</a>
                  </div>
                </div>
              </nav>
            <div className='container' style={{marginTop:'10%'}}>
              <div className="form-group">
                <label for="usr">Hygieia Root Path</label>
                <input type="text" placeholder='Enter the root path of Hygieia' value={this.state.rootPath} className="form-control" id="usr" onChange={this.rootPath.bind(this)}/>
              </div>

              <div className='row' style={{marginTop:"5%"}}>
                {
                  this.state.collectorsArray.map((item, key) => {
                    let checkbox = (<span className="glyphicon glyphicon-ok" style={{visibility:'hidden',zIndex: 1,color:"green", float: "right", marginRight:"15px", marginTop:"10px"}}></span>);
                    if(item.selected){
                      checkbox = (<span className="glyphicon glyphicon-ok" style={{zIndex: 1,color:"green", float: "right", marginRight:"15px", marginTop:"10px"}}></span>);
                    }
                    let cardContent = (<div><div className="clickCursor">
                            <div className="profile-box">
                                <img className="card-img-top rounded-circle" src={item.image} alt="Card image cap"/>
                            </div>
                            </div>
                            <div className="card-body text-center">
                              <div className="clickCursor">
                              <h4 className="card-title" style={{fontSize:"18px",paddingBottom:"10px",textTransform:'capitalize'}}>{item.name}</h4>
                            </div>
                          </div></div>);
                    return (<div key={key} className="col-md-3 col-sm-6 col-lg-3">
                      {(item.selected)?<div className="card card-01" style={{boxShadow:'2px 2px 20px rgba(33, 146, 59, 1)'}} onClick={this.toggle.bind(this,key)}>
                        {checkbox}
                        {cardContent}
                      </div>:<div className="card card-01" onClick={this.toggle.bind(this,key)}>
                        {checkbox}
                        {cardContent}
                      </div>}

                    </div>);
                  })

                }
              </div>
              <button type="button" className="btn btn-success" style={{float:'right'}} onClick={this.setRootPath.bind(this)}>Submit</button>
            </div>
  <div>
      <nav className="navbar" id="footer" >
       <div id = "ribbon" className="row footer-brand-colour">
             <div className="fbc-elem fbc-pink col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
             <div className="fbc-elem fbc-yellow col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
             <div className="fbc-elem fbc-blue col-xs-4 col-sm-4 col-md-4 col-lg-4"></div>
         </div>
            <p id="footerTextAllignment" >All Rights Reserved. &copy; Wipro Digital
          </p>
        </nav>
    </div>
    <ToastContainer ref='asd'
          toastMessageFactory={ToastMessageFactory}
          className='toast-top-center' style={{marginTop:'8%'}}/>
          </div>
        );
}
}

module.exports = Selection;
