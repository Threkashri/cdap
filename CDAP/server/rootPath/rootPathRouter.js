var express = require('express');
var router = express.Router();
var mongoose  = require('mongoose');
var component = require('./rootPathController');
router.get('/getRootPath',component.getRootPath);
router.post('/setRootPath',component.setRootPath);

module.exports = router;
