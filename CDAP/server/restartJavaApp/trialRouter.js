'use strict';
const router = require('express').Router();
const path = require('path');
const {collectors} = require('./collectorEntity');
const { spawn } = require('child_process');
var shell = require('shelljs');

router.get('/getCollectors', function(req, res) {
  collectors.find({}, function(err, result) {
    if(err)
      throw err;
    else
      res.send(result);
  })
});

router.post('/setSelection', function(req, res) {
  collectors.find({}, function(err, result) {
    if(err)
      throw err;
    else{
        let collectorsArray = JSON.parse(req.body.collectorsArray);
        result.map((item,index)=>{
          item.name = collectorsArray[index].name;
          item.image = collectorsArray[index].image;
          item.selected = collectorsArray[index].selected;
          item.lastRefreshedTime = collectorsArray[index].lastRefreshedTime;
          item.save(function(err) {
          if (err)
            throw err;
        });
        })
        res.send('success');
    }
  })
});

router.post('/restartJavaApp', function(req, res, next) {
  var tool = req.body.tool;
  collectors.find({
    'name': tool
  }, function(err, result) {
    if(result.length == 0){
      let collector = new collectors({
        name: tool,
        lastRefreshedTime: new Date().getTime()
      });
      collector.save(function(err) {
      if (err)
        throw err;
    });
    }
    else {
      result[0].lastRefreshedTime = new Date().getTime();
        console.log('result',result,new Date().getTime());
      result[0].save(function(err,data) {
      if (err)
        throw err;
        else {
            console.log('saved',data);
        }
    });
    }
  })
  let pathToBash = path.join(__dirname, `./bash/${tool}.sh`);
  shell.cd('C:/Hygieia-master/Hygieia-master/api/target');
  shell.ls('*.jar').forEach(function (file) {
    console.log('asdfg',file);
});

   const deploySh = spawn('sh', [ pathToBash ], {
   detach: true
 });
 console.log(deploySh);
res.send("success");

});


module.exports = router;
