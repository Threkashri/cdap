var bambooLogCtrl = {
        add: function(req, res) {
          let log = req.body.log.split('|');
          let key = log[4].split('-');
          let data = {
            project:{
              key:key[0],
              name:log[0]
            },
            plan:{
              key:key[1],
              name:log[1]
            },
            stage:log[3],
            job:{
              key:key[2],
              name:log[2]
            },
            executedTasks:log[5],
            totalTasks:log[6],
            build:key[3]
          }
          res.send(data);
        },
    }
module.exports = bambooLogCtrl;
