'use strict';
const logger = require('./../../applogger');
const router = require('express').Router();
const mongoose = require('mongoose');
const bambooLogCtrl = require('./bambooLogController');

router.post('/add', bambooLogCtrl.add);

module.exports = router;
